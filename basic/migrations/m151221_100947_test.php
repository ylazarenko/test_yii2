<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_100947_test extends Migration
{
    public function safeUp()
    {

        // создание таблицы `tour`
        $this->createTable(
                            '{{tour}}',
                            [
                                'id'        => 'pk',
                                'name'   => 'varchar(150) NOT NULL',
                            ]
                        );

        // создание таблицы `booking`
        $this->createTable(
                            '{{booking}}',
                            [
                                'id'        => 'pk',
                                'time'      => 'int(11) NOT NULL',
                                'address'   => 'varchar(250) NOT NULL',
                                'group_num' => 'int(11) NOT NULL',
                                'agency_id' => 'int(11) NOT NULL',
                                'adults'    => 'int(11) NOT NULL',
                                'childs'    => 'int(11) NOT NULL',
                                'infants'   => 'int(11) NOT NULL',
                                'tour_id'   => 'int(11) NOT NULL',
                                'pick_up'   => 'int(11) NOT NULL',
                                'drop_off'  => 'int(11) NOT NULL',
                                'tour_id'   => 'int(11) NOT NULL',
                            ]
                        );

        // создание связей таблицы `booking` с таблицей `tour`
        $this->createIndex('fk_booking_tour_id', '{{booking}}', 'tour_id');
        $this->addForeignKey('fk_booking_tour_id', '{{booking}}', 'tour_id', '{{tour}}', 'id', 'CASCADE', 'CASCADE');

        // создание таблицы `tour_fields`
        $this->createTable(
                            '{{tour_fields}}',
                            [
                                'id'        => 'pk',
                                'name'      => 'varchar(50) NOT NULL',
                                'type'      => 'int(6) NOT NULL',
                                'sort'      => 'int(2) NOT NULL',
                                'tour_id'   => 'int(11) NOT NULL',
                            ]
                        );

        // создание связей таблицы `tour_fields` с таблицей `tour`
        $this->createIndex('fk_tour_fields_tour_id', '{{tour_fields}}', 'tour_id');
        $this->addForeignKey('fk_tour_fields_tour_id', '{{tour_fields}}', 'tour_id', '{{tour}}', 'id', 'CASCADE', 'CASCADE');

        $this->insert(
                        'tour',
                        [
                            'id' => 1,
                            'name' => 'Similans',
                        ]
                    );

        $this->insert(
                        'tour_fields',
                        [
                            'id'        => 1,
                            'name'      => 'test',
                            'type'      => 1,
                            'sort'      => 0,
                            'tour_id'   => 1,
                        ]
                    );

        $this->insert(
                        'tour_fields',
                        [
                            'id'        => 2,
                            'name'      => 'test2',
                            'type'      => 1,
                            'sort'      => 0,
                            'tour_id'   => 1,
                        ]
                    );

        $this->insert(
                        'tour_fields',
                        [
                            'id'        => 3,
                            'name'      => 'test3',
                            'type'      => 1,
                            'sort'      => 0,
                            'tour_id'   => 1,
                        ]
                    );

    }

    public function safeDown()
    {
        $this->dropTable('{{tour_fields}}');
        $this->dropTable('{{booking}}');
        $this->dropTable('{{tour}}');
    }

}
