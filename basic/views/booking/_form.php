<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vakorovin\datetimepicker\Datetimepicker;
?>

<div class="booking-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tour_id')->dropDownList($listTour); ?>


    <?= $form->field($model, 'time')->widget(Datetimepicker::className(),[
        'options' => [
            'lang' => 'ru',
            'autocomplete' => 'off',
        ]
        ]);
    ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_num')->textInput() ?>

    <?= $form->field($model, 'agency_id')->textInput() ?>

    <?= $form->field($model, 'adults')->textInput() ?>

    <?= $form->field($model, 'childs')->textInput() ?>

    <?= $form->field($model, 'infants')->textInput() ?>


    <?= $form->field($model, 'pick_up')->textInput() ?>

    <?= $form->field($model, 'drop_off')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
