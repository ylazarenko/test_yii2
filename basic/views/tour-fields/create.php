<?php

use yii\helpers\Html;


$this->title = 'Create Tour Fields';
$this->params['breadcrumbs'][] = ['label' => 'Tour Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-fields-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listTour' => $listTour,
    ]) ?>

</div>
