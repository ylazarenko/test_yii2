<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TourFields */

$this->title = 'Update Tour Fields: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tour Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tour-fields-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listTour' => $listTour,
    ]) ?>

</div>
