<?php

namespace app\widgets\CustomFieldWidget;

use yii\helpers\Html;

class CustomFieldWidget extends \yii\bootstrap\Widget
{

	public $itemList = '';
	public $modelName = 'customField';

	public function run()
	{
		$nameDiv = 'customFields';
		$js =   '<script>
                    $(function(){

                        $("#' . $nameDiv . '").on("click", ".remove",function(){
                            if ($("#' . $nameDiv . '").children("table").length > 1) {
                                $(this).parent().parent().parent().parent().remove();
                            } else {
                                return false;
                            }
                        });

                        $(".add").click(function(){
                            var nextNumber = $("#counter").val();
                            var table = $("#' . $nameDiv . '").children("table").last();
                            var cloneTable = $(table).clone();
                            $("#' . $nameDiv . '").append($(cloneTable));
                            $("#' . $nameDiv . '").append("<hr>");
                            var inputs = $($("#' . $nameDiv . '").children("table").last()).find("input");
                            inputs.each(function(){
                                var idElem = $(this).attr("id");
                                var patternNumber = /\d+_(\w+)$/
                                var arr = patternNumber.exec(idElem);
                                if (arr != null) {
                                    $(this).attr("id", "' . $this->modelName . '_" + nextNumber + "_" + arr[1]);
                                    $(this).attr("name", "' . $this->modelName . '[" + nextNumber + "][" + arr[1] + "]");
                                    if ($(this).attr("type") == "text") {
                                        $(this).val("");
                                    } else if ($(this).attr("type") == "checkbox") {
                                        $(this).prop("checked", false);
                                    }
                                    if ($(this).attr("type") == "hidden") {
                                        $(this).remove();
                                    }
                                }
                            });
                            nextNumber++;
                            $("#counter").val(nextNumber);
                        });

                    });
                </script>';
        echo $js;

		if (empty($this->itemList)) {
			$tableCustomField = '
				<div id="' . $nameDiv . '">' .
				Html::hiddenInput('counter', '1', ['id' => 'counter'])
					. '<table>
						<tr>
							<th>Имя</th>
							<td style="padding: 10px 0px"> ' .
								Html::textInput($this->modelName. '[0][name]', '', ['class' => 'form-control col-lg-6', 'id' => $this->modelName . '_0_name'])
							. '</td>
						</tr>
						<tr>
							<th>Тип</th>
							<td style="padding: 10px 0px">' .
								Html::textInput($this->modelName . '[0][type]', '', ['class' => 'form-control col-lg-6', 'id' => $this->modelName . '_0_type'])
							. '</td>
						</tr>
						<tr>
							<th>Сортировка</th>
							<td style="padding: 10px 0px">' .
								Html::textInput($this->modelName . '[0][sort]', '', ['class' => 'form-control col-lg-6', 'id' => $this->modelName . '_0_sort'])
							. '</td>
						</tr>
						<tr>
							<td colspan="2">' .
								Html::tag('span', 'Удалить', ['style' => 'color: darkred; cursor:pointer', 'class' => 'remove'])
							. '</td>
						</tr>
					</table></div><div>' .
						Html::tag('span', 'Добавить еще', ['class' => 'add', 'style' => 'color: darkgreen; cursor: pointer'])
				. '</div>';
		} else {
			$count = count($this->itemList);
			$tableCustomField = '
				<div id="' . $nameDiv . '">' . Html::hiddenInput('counter', $count, ['id' => 'counter']);
				$iter = 0;
				foreach($this->itemList as $item) {
					$tableCustomField .= '
						<table style="width:80%;">
							<tr>
								<th>Имя</th>
								<td style="padding: 3px 0px"> ' .
									Html::textInput($this->modelName . '[' . $iter . '][name]', $item->name, ['class' => 'form-control col-lg-6', 'id' => $this->modelName . '_' . $iter . '_name'])
								. '</td>
							</tr>
							<tr>
								<th>Тип</th>
								<td style="padding: 3px 0px">' .
									Html::textInput($this->modelName . '[' . $iter . '][type]', $item->type, ['class' => 'form-control col-lg-6', 'id' => $this->modelName . '_' . $iter . '_type'])
								. '</td>
							</tr>
							<tr>
								<th>Сортировка</th>
								<td style="padding: 3px 0px">' .
									Html::textInput($this->modelName . '[' . $iter . '][sort]', $item->sort, ['class' => 'form-control col-lg-6', 'id' => $this->modelName . '_' . $iter . '_sort'])
								. '</td>
							</tr>
							<tr>
								<td colspan="2">' .
									Html::tag('span', 'Удалить', ['style' => 'color: darkred; cursor:pointer', 'class' => 'remove']) .
									Html::hiddenInput($this->modelName . '[' . $iter . '][id]', $item->id, ['id' => $this->modelName . '_' . $iter . '_id'])
								. '</td>
							</tr>
						</table><hr>';
					$iter++;
				}
			$tableCustomField .= '</div><div>' . Html::tag('span', 'Добавить еще', ['class' => 'add', 'style' => 'color: darkgreen; cursor: pointer']) . '</div>';
		}
		echo $tableCustomField;
	}
}