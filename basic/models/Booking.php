<?php

namespace app\models;

use Yii;

class Booking extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'booking';
    }

    public function rules()
    {
        return [
            [['time', 'address', 'group_num', 'agency_id', 'adults', 'childs', 'infants', 'tour_id', 'pick_up', 'drop_off'], 'required'],
            [['group_num', 'agency_id', 'adults', 'childs', 'infants', 'tour_id', 'pick_up', 'drop_off'], 'integer'],
            [['time'], 'safe'],
            [['time'], 'date', 'format' => 'yyyy/MM/dd H:i'],
            [['address'], 'string', 'max' => 250]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'time'      => 'Время',
            'address'   => 'Адрес',
            'group_num' => 'Номер группы',
            'agency_id' => 'Агенство',
            'adults'    => 'Adults',
            'childs'    => 'Childs',
            'infants'   => 'Infants',
            'tour_id'   => 'Тур',
            'pick_up'   => 'Pick Up',
            'drop_off'  => 'Drop Off',
        ];
    }

    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tour_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->time = strtotime($this->time);
            return true;
        } else {
            return false;
        }
    }
}
