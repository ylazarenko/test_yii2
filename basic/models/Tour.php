<?php

namespace app\models;

use Yii;

class Tour extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tour';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 150]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'name'  => 'Название тура',
        ];
    }

    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['tour_id' => 'id']);
    }

    public function getTourFields()
    {
        return $this->hasMany(TourFields::className(), ['tour_id' => 'id']);
    }
}
