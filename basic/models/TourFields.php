<?php

namespace app\models;

use Yii;

class TourFields extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tour_fields';
    }


    public function rules()
    {
        return [
            [['name', 'type', 'tour_id'], 'required'],
            [['type', 'sort', 'tour_id'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Название',
            'type'      => 'Тип',
            'sort'      => 'Сортировка',
            'tour_id'   => 'Тур',
        ];
    }


    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['id' => 'tour_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $modelTourField = TourFields::find()->where(['tour_id' => $this->tour_id])->one();
                if (!empty($modelTourField)) {
                    $this->sort = $modelTourField->sort;
                } else {
                    $this->sort = (!empty($this->sort)) ? $this->sort : -1;
                }
            } else {
                $modelTourField = TourFields::find()->andWhere('tour_id = :tourId && id <> :id',[":tourId" => $this->tour_id, ":id" => $this->id])->one();
                if (!empty($modelTourField)) {
                    if (empty($this->sort)) {
                        $this->sort = $modelTourField->sort;
                    } else {
                        foreach($modelTourField as $model) {
                            $model->sort = $this->sort;
                            $model->save();
                        }
                    }
                } else {
                    $this->sort = (!empty($this->sort)) ? $this->sort : -1;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
