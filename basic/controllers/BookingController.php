<?php

namespace app\controllers;

use Yii;
use app\models\Booking;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Tour;
use yii\data\ArrayDataProvider;
use app\models\TourFields;

class BookingController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $modelsBooking = Booking::find()->all();

        $models = TourFields::find()->orderBy('sort')->all();

        $p = [];

        $tmpModels = [];

        foreach ($models as $model) {
            if (in_array($model->tour_id, $p)) {
                continue;
            } else {
                $p[] = $model->tour_id;
            }
        }

        $listDataBooking = ArrayHelper::map($modelsBooking, 'id', 'id');

        foreach ($p as $tourId) {
            foreach ($modelsBooking as $modelBooking) {
                if ($modelBooking->tour_id == $tourId) {
                    $tmpModels[$modelBooking->id] = $modelBooking;
                }
            }
        }

        foreach ($listDataBooking as $tour_id => $idBooking) {
            echo $idBooking . "<br>";
            if (!array_key_exists($tour_id, $tmpModels)) {
                $tmpModels[$idBooking] = Booking::find()->andWhere(['=', 'id', $idBooking])->one();
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $tmpModels,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Booking();
        $listTour = ArrayHelper::map(Tour::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'listTour' => $listTour,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $listTour = ArrayHelper::map(Tour::find()->all(), 'id', 'name');
        $model->time = date('Y/m/d H:i', $model->time);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'listTour' => $listTour,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
